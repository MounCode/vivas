<!DOCTYPE html>
<html lang="ru">
[[$head]]
<body>
    [[$header]]
    <div class="container">
        <div class="main_slider_block">
            <div class="main_slider">
                [[getImageList?
                    &tvname=`mainSlider`
                    &tpl=`mainSliderTpl`
                    &limit=`100`
                    &docid=`1`
                ]]
            </div>
        </div>
    </div>
    <div class="container">
        <div class="main">
            <h1>[[*pagetitle]]</h1>
            <div class="catalog_block">
                <div class="catalog_box table_view row sb main_page_catalog">
                    [[!getResources? &parents=`9` &limit=`0` &tpl=`mainPagecatalogItem` &includeTVs=`1` &sortdir=`ASC`]]
                </div>
            </div>
            <div class="content_box">
                [[*introtext]]
            </div>
            [[$promo_block_1]]
            <div class="content_box">
                [[*content]]
            </div>
        </div>
        [[$footer]]
    </div>
    [[$scripts]]
</body>
</html>