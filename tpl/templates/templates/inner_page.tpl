<!DOCTYPE html>
<html lang="ru">
[[$head]]
<body>
    [[$header]]
    <div class="container">
        <div class="main">
            <h1>[[*pagetitle]]</h1>
            <div class="content_box">
                [[*content]]
            </div>
        </div>
        [[$footer]]
    </div>
    [[$scripts]]
</body>
</html>