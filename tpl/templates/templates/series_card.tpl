<!DOCTYPE html>
<html lang="ru">
[[$head]]
<body>
    [[$header]]
    <div class="container">
        <div class="main">
            <div class="breadcrumbs_box">
                [[Breadcrumbs? &crumbSeparator=`` &homeCrumbTitle=`Главная` &currentAsLink=`0` &showCurrentCrumb=`1`]]
            </div>
            <h1>Элит-эксклюзив</h1>
            <!--PRODUCT INFO BLOCK-->
            <div class="product_block row simple_row">
                <div class="one_third product_illustration row sb">
                    <div class="main_img">
                        <div class="img_box">
                            <img src="tpl/img/product.png" alt="product" />    
                        </div>
                        <div class="button_block">
                            <span class="checked">Спереди</span>
                            <span>Сбоку</span>
                            <span>Открытая дверца</span>
                            <span>Задняя стенка</span>
                        </div>
                    </div>
                    <div class="box_counter">
                        <p>Ячеек</p>
                        <div class="counter">
                            <div>1</div>
                            <div class="selected">2
                                <div class="flag">
                                    Осталось 16 штук
                                    <span>От 200 рублей</span>
                                </div>
                            </div>
                            <div>3
                            </div>
                            <div>4</div>
                            <div>5</div>
                            <div>6
                                <div class="flag">
                                    Осталось 19 штук
                                    <span>От 290 рублей</span>
                                </div>
                            </div>
                            <div>7</div>
                            <div>8</div>
                            <div>9
                                <div class="flag">
                                    Осталось 34 штук
                                    <span>От 780 рублей</span>
                                </div>
                            </div>
                            <div>10</div>
                            <div>11</div>
                            <div>12</div>
                            <div>13</div>
                            <div>14</div>
                        </div>
                    </div>
                </div>
                <div class="two_third product_info">
                    <p class="block_header">Характеристики серии</p>
                    <div class="product_param row sb">
                        <div class="item">
                            <span>Ширина секции</span>
                            <p>330 мм</p>
                        </div>
                        <div class="item">
                            <span>Заводская окраска</span>
                            <p>RAL 7035</p>
                        </div>
                        <div class="item">
                            <span>Толщина металла двери</span>
                            <p>0,8 мм</p>
                        </div>
                        <div class="item">
                            <span>Толщина металла корпуса</span>
                            <p>0,8 мм</p>
                        </div>
                    </div>
                    <p class="description">
                        Почтовые ящики серии ЯП предназначены для многоквартирных домов элитного класса, бизнес-центров, гостиниц и других учреждений. Каждое отделение оборудовано замком и номером. Такие ящики являются устойчивыми к коррозии, обладают высоким уровнем прочности и идеально вписываются в отделку подъезда с использование мрамора и гранита.
                    </p>
                    <a href="#" class="download_link">Паспорт продукции</a>
                    <div class="color_block">
                        <p class="subheader">Возможности по перекраске серии</p>
                        <div class="body_color_box">
                            <div class="row sb">
                                <p class="color_info">Цвет корпуса <span>RAL 8234</span></p>
                                <p class="drop_param">Вернуть заводской цвет RAL 7035</p>
                            </div>
                            <div class="color_bar">

                            </div>
                        </div>
                        <div class="door_color_box">
                            <div class="row sb">
                                <p class="color_info">Цвет корпуса <span>RAL 8234</span></p>
                                <p class="drop_param">Вернуть заводской цвет RAL 7035</p>
                            </div>
                            <div class="color_bar">

                            </div>
                        </div>
                    </div>
                    <div class="promo_block_3 row sb">
                        <div class="item">
                            <div class="img_box">
                                <img src="tpl/img/shape3.png" alt="promo_img">
                            </div>
                            <p>Мы делаем сварные ящики,<br/> а не используем заклепки,<br/> которые потом обычно<br/> расшатываются<br/> и вылетают.</p>
                        </div>
                        <div class="item">
                            <div class="img_box">
                                <img src="tpl/img/shape4.png" alt="promo_img">
                            </div>
                            <p>Петли на задней стенке<br/> «зеркальны» —<br/>это противосъёмное<br/>крепление, которое не снять<br/>без участия монтажника</p>
                        </div>
                        <div class="item">
                            <div class="img_box">
                                <img src="tpl/img/shape2.png" alt="promo_img">
                            </div>
                            <p>Вся продукция имеет<br/>качественное порошковое<br/>покрытие в несколько<br/>слоёв. Прокрашивается всё,<br/>даже швы</p>
                        </div>
                        <div class="item">
                            <div class="img_box">
                                <img src="tpl/img/shape1.png" alt="promo_img">
                            </div>
                            <p>Замки уже установлены<br/>в двери ящиков и установка<br/>включена в стоимость.<br/>В комплекте с замками мы<br/>даем два ключа</p>
                        </div>
                    </div>
                    <p class="subheader">Параметры выбранного ящика</p>
                    <div class="selected_product_param row sb">
                        <div class="item">
                            <span>Базовая стоимость</span>
                            <p>150 <i class="fa fa-rub" aria-hidden="true"></i></p>
                        </div>
                        <div class="item">
                            <span>Ячеек</span>
                            <p>5 шт.</p>
                        </div>
                        <div class="item">
                            <span>Высота×Ширина×Глубина</span>
                            <p>623×350×150 мм</p>
                        </div>
                        <div class="item">
                            <span>Объем</span>
                            <p>0,03 м³</p>
                        </div>
                        <div class="item">
                            <span>Вес брутто с замком</span>
                            <p>7,1 кг</p>
                        </div>
                    </div>
                    <p class="subheader">Дополнительные опции</p>
                    <div class="additional_options_block row sb">
                        <div class="options">
                            <div class="item row sb baseline">
                                <div class="check_box">
                                    <input type="checkbox" class="checkbox" id="checkbox1" />
                                    <label for="checkbox1">Маркировка</label>
                                </div>
                                <p class="add_price">—</p>
                            </div>
                            <div class="button_block">
                                <span class="checked">Без маркировки</span>
                                <span>Маркер по трафарету</span>
                                <span>Гелевые шилды</span>
                                <span>Алюминиевые таблички</span>
                                <span>Краска по трафарету</span>
                                <span>Лазерная гравировка</span>
                                <span>Самоклеящаяся пленка</span>
                            </div>
                            <div class="item row sb baseline">
                                <div class="check_box">
                                    <input type="checkbox" class="checkbox" id="checkbox2" />
                                    <label for="checkbox2">Двери из нержавейки</label>
                                </div>
                                <p class="add_price">+ 100 <i class="fa fa-rub" aria-hidden="true"></i></p>
                            </div>
                            <div class="item row sb baseline">
                                <div class="check_box">
                                    <input type="checkbox" class="checkbox" id="checkbox3" />
                                    <label for="checkbox3">Декорирование пленкой</label>
                                </div>
                                <p class="add_price">—</p>
                            </div>
                            <div class="item row sb baseline">
                                <div class="check_box">
                                    <input type="checkbox" class="checkbox" id="checkbox4" />
                                    <label for="checkbox4">Гравировка</label>
                                </div>
                                <p class="add_price">—</p>
                            </div>
                            <div class="item row sb baseline">
                                <div class="check_box">
                                    <input type="checkbox" class="checkbox" id="checkbox5" />
                                    <label for="checkbox5">Требуется перекраска</label>
                                </div>
                                <p class="add_price">—</p>
                            </div>
                        </div>
                        <div class="image">
                            <img src="tpl/img/rectangle-22.png" alt="ex">
                            <p class="title">
                                Пример маркировки алюминиевыми табличками
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="final_price_block row simple_row">
                <div class="one_third">
                    <div class="img_box">
                        
                    </div>
                </div>
                <div class="two_third">
                    <div class="price_info">
                        <div class="row fs">
                            <p class="final_price">250 <i class="fa fa-rub" aria-hidden="true"></i></p>
                            <a href="/" class="blue_button">Заказать</a>    
                        </div>
                        <p class="warn">Внимание: данный товар поставляется только партией от 25 изделий</p>
                        <a href="/" class="more">Как сделать еще дешевле?</a>
                    </div>
                </div>
            </div>
            <!--PRODUCT INFO BLOCK END-->
            [[$promo_block_1]]
            [[$product_carousel]]
            [[$promo_block_4]]
            <div class="content_box">
                <div class="compare_tables">
                    <h3>Сводная таблица параметров ящиков серии «Эконом»</h3>
                    <div class="row sb simple_row">
                        <div class="one_half">
                            <table>
                                <thead>
                                    <tr>
                                        <td>Число ячеек</td>
                                        <td>Высота×Ширина×Глубина,мм</td>
                                        <td>Вес с замком в упаковке, кг</td>
                                        <td>Объем, м³</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>251×350×150</td>
                                        <td>2,1</td>
                                        <td>0,013</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>344×350×150</td>
                                        <td>3,3</td>
                                        <td>0,017</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>437×350×150</td>
                                        <td>5,8</td>
                                        <td>0,02</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>530×350×150</td>
                                        <td>5,8</td>
                                        <td>0,02</td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>623×350×150</td>
                                        <td>7,1</td>
                                        <td>0,03</td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>716×350×150</td>
                                        <td>8,4</td>
                                        <td>0,03</td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>806×350×150</td>
                                        <td>9,4</td>
                                        <td>0,04</td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>899×350×150</td>
                                        <td>10,6</td>
                                        <td>0,04</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="one_half">
                            <table>
                                <thead>
                                    <tr>
                                        <td>Число ячеек</td>
                                        <td>Высота×Ширина×Глубина,мм</td>
                                        <td>Вес с замком в упаковке, кг</td>
                                        <td>Объем, м³</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>9</td>
                                        <td>995×350×150</td>
                                        <td>11,8</td>
                                        <td>0,05</td>
                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>1087×350×150</td>
                                        <td>13,0</td>
                                        <td>0,05</td>
                                    </tr>
                                    <tr>
                                        <td>11</td>
                                        <td>1181×350×150</td>
                                        <td>14,2</td>
                                        <td>0,06</td>
                                    </tr>
                                    <tr>
                                        <td>12</td>
                                        <td>1274×350×150</td>
                                        <td>15,4</td>
                                        <td>0,06</td>
                                    </tr>
                                    <tr>
                                        <td>13</td>
                                        <td>1367×350×150</td>
                                        <td>16,6</td>
                                        <td>0,07</td>
                                    </tr>
                                    <tr>
                                        <td>14</td>
                                        <td>1460×350×150</td>
                                        <td>17,8</td>
                                        <td>0,07</td>
                                    </tr>
                                    <tr>
                                        <td>15</td>
                                        <td>1533×350×150</td>
                                        <td>19,0</td>
                                        <td>0,08</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <a href="/" class="simple_button compare_button">
                        <img src="tpl/img/noun-669299-cc.png" alt="">
                        Сравнить все серии ящиков
                    </a>
                </div>
            </div>
            [[$promo_block_2]]
        </div>
        [[$footer]]
    </div>
    [[$scripts]]
</body>
</html>