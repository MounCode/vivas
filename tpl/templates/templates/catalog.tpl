<!DOCTYPE html>
<html lang="ru">
[[$head]]
<body>
    [[$header]]
    <div class="container">
        <div class="main">
            <div class="breadcrumbs_box">
                [[Breadcrumbs? &crumbSeparator=`` &homeCrumbTitle=`Главная` &currentAsLink=`0` &showCurrentCrumb=`1`]]
            </div>
            <div class="catalog_block">
                <div class="button_block row sb">
                    <div class="categories">
                        <span>Все серии</span>
                        <span>Для многоквартирных домов</span>
                        <span>Уличные почтовые ящики</span>
                        <span>Почтовые ящики с прозрачными дверцами</span>
                    </div>
                    <div class="catalog_view">
                        <span class="block">Плитка с изображениями</span>
                        <span class="table checked">Таблица со сравнением</span>
                    </div>
                </div>
                <h1>[[LongOrPageTitle]]</h1>
                <div class="catalog_box table_view row sb">
                    <div class="headers">
                        <p class="opt1">Код серии</p>
                        <p class="opt2">Цена</p>
                        <p class="opt3">Толщина металла корпуса</p>
                        <p class="opt4">Толщина металла двери</p>
                        <p class="opt5">Глубина ячейки</p>
                        <p class="opt6">Ширина секции</p>
                        <p class="opt7">Тип металла</p>
                        <p class="opt8">Способ открытия дверцы</p>
                        <p class="opt9">Конструкция низа ящика</p>
                        <p class="opt10">Тип загрузки ящика</p>
                        <p class="opt11">Способы окраски</p>
                        <p class="opt12">Варианты маркировки</p>
                    </div>
                    [[!getResources? &parents=`[[*id]]` &limit=`0` &tpl=`catalogItem` &includeTVs=`1` &sortdir=`ASC`]]
                </div>
                <div class="content_box">
                    [[*content]]
                </div>
            </div>
        </div>
        [[$footer]]
    </div>

    [[$scripts]]
</body>
</html>