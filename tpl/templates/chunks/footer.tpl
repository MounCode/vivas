<footer>
    <div class="phone_row row fb">
        <div class="phone">
            <p>Звоните на горячую линию</p>
            <p><span>8 800 555 24 64</span></p>
        </div>
        <div class="button">
            <a href="/" class="red_button">Заказать звонок</a>
        </div>
    </div>
    <div class="info_block row sb">
        <div class="logo_box">
            <div class="logo">
                <a href="/">
                    <img src="tpl/img/logo.svg" alt="ВИВАС">
                </a>
                <p>Завод полного цикла по производству<br/>почтовых ящиков</p>
            </div>
            <div class="worktime">
                <span>Понедельник — четверг</span>
                <p>8:00 — 16:45</p>
                <p class="warn">Отгрузка товара до 16:30</p>
            </div>
            <div class="worktime">
                <span>Пятница</span>
                <p>8:00 — 15:45</p>
                <p class="warn">Отгрузка товара до 15:30</p>
            </div>
        </div>
        <div class="contact_info">
            <p class="block_header">Основные офисы</p>
            <div class="item">
                <p>Санкт-Петербург</p>
                <span>переулок Челиева, дом 7</span>
                <span>+7 (861) 991-19-68</span>
            </div>
            <div class="item">
                <p>Москва</p>
                <span>улица Лобненская, дом 18</span>
                <span>+7 (495) 401-52-70</span>
            </div>
            <div class="item">
                <p>Краснодар</p>
                <span>улица Покрышкина,дом 2/4</span>
                <span>+7 (861) 991-19-68</span>
            </div>
        </div>
        <div class="contact_info">
            <p class="block_header">Дополнительные офисы</p>
            <div class="item">
                <p>Новосибирск</p>
                <span>улица Авиастроителей, дом 30</span>
                <span>+7 (383) 349-58-31</span>
            </div>
            <div class="item">
                <p>Тюмень</p>
                <span>улица Комменистическая, дом 47</span>
                <span>+7 (495) 401-52-70</span>
            </div>
            <div class="item">
                <p>Омск</p>
                <span>улица 10 лет Октября, дом 180</span>
                <span>+7 (381) 279-03-26</span>
            </div>
        </div>
        <div class="contact_info">
            <p class="block_header">Дополнительные офисы</p>
            <div class="item">
                <p>Ростов-на-Дону</p>
                <span>улица Малиновского, дом 3</span>
                <span>+7 (863) 285-06-13</span>
            </div>
            <div class="item">
                <p>Казань</p>
                <span>улица Магистральная, дом 4</span>
                <span>+7 (843) 528-07-85</span>
            </div>
        </div>
    </div>
    <div class="catalog_menu">
        [[Wayfinder? startId=`0` &outerClass=`row sb` &excludeDocs=`1,2,3,4,5,6,7,8` &level=`1`]]
    </div>
    <nav class="bottom_nav">
        [[Wayfinder? startId=`0` &excludeDocs=`9,23,24,25` &level=`1`]]
    </nav>
    <div class="copyright">
        Вся представленная на сайте информаци носит информационный характер и не является публичной офертой
    </div>
</footer>