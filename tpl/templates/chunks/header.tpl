<header>
    <div class="container">
        <nav class="top_nav">
            [[Wayfinder? startId=`0` &excludeDocs=`9,23,24,25` &level=`1`]]
        </nav>
        <div class="info_block">
            <div class="row sb">
                <div class="logo_box">
                    <a href="[[~1]]">
                        <img src="tpl/img/logo.svg" alt="ВИВАС">
                        <p class="slogan">Красивые, оригинальные<br/>и качественные почтовые ящики<br/> по доступным ценам</p>
                    </a>
                </div>
                <div class="worktime_box">
                    <div class="worktime">
                        <span>Пн-Чт</span>
                        <p><img src="tpl/img/clock.svg" alt="worktime_icon" />8:00-16:45</p>
                    </div>
                    <div class="worktime">
                        <span>Пт</span>
                        <p><img src="tpl/img/clock.svg" alt="worktime_icon" />8:00-15:30</p>
                    </div>
                    <p class="red_button">Заказать звонок</p>
                    <a href="mailto:Zao-vivas@yandex.ru" class="simple_button">Zao-vivas@yandex.ru</a>
                </div>
                <div class="phone_block row sb">
                    <div class="phone_box main_phones">
                        <span>Бесплатно по Росии</span>
                        <p class="big_phone">8 800 555-24-64</p>
                        <span>Москва</span>
                        <p>+7 (495) 401-52-70</p>
                        <span>Краснодар</span>
                        <p>+7 (861) 991-19-68</p>
                        <span>Санкт-Петербург</span>
                        <p>+7 (861) 991-19-68</p>
                    </div>
                    <div class="additional_phones">
                        <p class="block_header">Дополнительные офисы</p>
                        <div class="row fe">
                            <span>Новосибирск</span>
                            <p>+7 (383) 349-58-31</p>
                        </div>
                        <div class="row fe">
                            <span>Ростов-на-Дону</span>
                            <p>+7 (863) 285-06-13</p>
                        </div>
                        <div class="row fe">
                            <span>Тюмень</span>
                            <p>+7 (345) 256-85-17</p>
                        </div>
                        <div class="row fe">
                            <span>Казань</span>
                            <p>+7 (843) 528-07-85</p>
                        </div>
                        <div class="row fe">
                            <span>Омск</span>
                            <p>+7 (381) 279-03-26</p>
                        </div>
                        <p class="delivery">Доставка в любой город России</p>
                    </div>
                </div>
            </div>
        </div>
        <nav class="catalog_menu">
            [[Wayfinder? startId=`0` &outerClass=`row sb` &excludeDocs=`1,2,3,4,5,6,7,8` &level=`1`]]
        </nav>
    </div>
</header>