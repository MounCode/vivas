<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=1144">
    <base href="[[!++site_url]]" />
    [[If?
       &subject=`[[*site_title]]`
       &operator=`!=`
       &operand=``
       &then=`<title>[[*site_title]]</title>`
       &else=`<title>[[*pagetitle]]</title>`
    ]]
    <meta name="description" content="[[*site_description]]" />
    <meta name="keywords" content="[[*site_keywords]]" />

    <link rel="stylesheet" href="tpl/libs/owl.carousel/docs/assets/owlcarousel/assets/owl.theme.default.min.css" />
    <link rel="stylesheet" href="tpl/libs/fancybox/dist/jquery.fancybox.min.css" />
    <link rel="stylesheet" href="tpl/less/main.css" />
</head>