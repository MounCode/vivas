$(document).ready(function(){
    // Переменная ширины экрана для адаптивки
    let mobileWidth = $(window).width();
    
    if (mobileWidth <= 765){
        // Настройка адаптивки
        $('.mobile_menu').click(function(){
            $(this).toggleClass('closer');
            $('.menu_box ul').toggleClass('showIt'); 
        });  
    }
    
    // Выравнивание строк табличного представления каталога
    
    // Поиск строк
    function cellFinder(){
        $(".catalog_box p[class*='opt']").each(function(){
            var thisCell = '.'+$(this).attr('class');
            cellHighter(thisCell);
        });
    }
    
    // Выравнивание строк
    function cellHighter(cell){
        var maxHeight = 0;
        $(cell).each(function(){
            
            if ($(this).outerHeight() > maxHeight){
                maxHeight = $(this).outerHeight();
            }
        });
        $(cell).css('height', maxHeight);
    };
    cellFinder();
    
    // Переключение вида каталога
    $('.catalog_view .block').click(function(){
        $('.catalog_box').addClass('block_view').removeClass('table_view');
        $(this).addClass('checked');
        $('.catalog_view .table').removeClass('checked');
    });
    
    $('.catalog_view .table').click(function(){
        $('.catalog_box').addClass('table_view').removeClass('block_view');
        $(this).addClass('checked');
        $('.catalog_view .block').removeClass('checked');
    });
    
});

//Карусель акций на главной
$(".main_slider").owlCarousel({
    lazyLoad:true,
    loop: true,
    auto: true,
    margin: 0,
    items:3,
    dots: true,
    autoWidth:true,
    nav:false
});


//Галерея серии на странице продукта
$(".product_carousel").owlCarousel({
    lazyLoad:true,
    loop: true,
    auto: true,
    margin: 30,
    items:3,
    dots: true,
    nav:false
});


